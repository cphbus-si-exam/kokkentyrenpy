from flask import Flask, render_template, redirect, url_for, session, request, flash, jsonify
import datetime
import json
import random
import requests
from catering import caterings

app = Flask(__name__)

eventid =''
normalmeal = ''
specialmeal = ''
type = [ ]
startdate = ''
enddate = ''


@app.route('/')
def home():
    return render_template('home.html')

@app.route('/booking', methods=['POST','GET'])
def booking():
	if request.method =='POST':
		incomingdata = request.data
		ndata=json.loads(incomingdata)
		#print(ndata['eventid'])

		eventid = ndata['eventid']
		normalmeal = ndata['normalmeal']
		specialmeal = ndata['specialmeal']
		type = ndata['type']
		startdate = ndata['startdate']
		enddate = ndata['enddate']
		requesttype = ndata['request_type']	
		print(requesttype)
		
		cateringobj = caterings(eventid,normalmeal,specialmeal,type,startdate,enddate,requesttype)
		
		status = 'true'
		addinfo = ''
		msg = []
		desc = ''

		desclist = [' Contactperson: Peppe Corleone',' Contactperson: Monica Arabiata',' Contactperson: Henrik Bruschetta']
		
		for mealtype in cateringobj.get_type():

			if mealtype=='morning':
				desc+='Breakfast: Freshly baked bread, corn and fruits. '
			elif mealtype=='noon':
				desc+=' Noon: traditional danish lunch buffet. '
			elif mealtype=='dinner':
				desc+=' Dinner: italien specialities.'
			else:
				desc+=''  

		desc+= random.choice(desclist)
		if cateringobj.get_normalmeal() == False:
			msg.append('Maximum limit of normalmeal is 2000.')
			status = 'false'
			desc = ''
		
		if cateringobj.get_specialmeal() == False:
			msg.append('Maximum limit of specialmeals is 100.')
			status = 'false'
			desc = ''	
								
		addinfo = ' '.join(msg)
			
		replypackage = {'eventid': eventid, 'description': addinfo, 'status': status, 'additional_info': desc,'request_type':requesttype}
		
		print(replypackage)
		return json.dumps(replypackage)

	else:
		return ""
			
    
   
@app.route('/menu')
def menu():
    return render_template('menu.html')

@app.route('/success/', methods=['GET'])
def success():

	return jsonify({'reply':'true'})

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)

