from datetime import datetime

class caterings:

    eventid =''
    normalmeal = ''
    specialmeal = ''
    type = [ ]
    startdate = '' 
    enddate=''
    requesttype = ''

    def __init__(self, eventid, normalmeal, specialmeal, type, startdate, enddate,requesttype):
        self.eventid = eventid
        self.normalmeal = normalmeal
        self.specialmeal = specialmeal
        self.type = type
        self.startdate = startdate
        self.enddate = enddate
        self.requesttype = requesttype

    def get_eventid(self):
        return self.eventid

    def get_normalmeal(self):
        if self.normalmeal<0 or self.normalmeal> 2000:
            return False
        else:
            return self.normalmeal

    def get_specialmeal(self):
        if self.specialmeal<0 or self.specialmeal> 100:
            return False
        else:
            return self.specialmeal
      
    def get_type(self):
        return self.type

    def get_stardate(self):
        return self.startdate
        
    def get_enddate(self):
        return self.enddate

    def get_requesttype(self):
        return self.requesttype

    

        